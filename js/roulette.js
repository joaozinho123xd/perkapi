$('document').ready(function() {
  initialize();
  getCurrentPerks();
  rollClick();
  setTimeout(function () {
    makeTokens();
  }, 7000);
});

//proteção: número inteiro = número de roletadas antes do próximo sorteio
//daily: não poderá sair no mesmo dia
//weekly: não poderá sair na mesma semana
//monthly: não poderá sair no mesmo mês

var perks = [];
var perkLen = 0;

var tokens = [];

var sorteados = getCurrentSorteados();


var sorteadosLen = 0;


function makeTokens(){
  var i;
  var j;
  for(i = 0; i < perks.length; i++){
    for(j = 0; j < perks[i].perk_chance; j++){
      tokens.push(perks[i]);
    }
  }
}

function randomName() {
  var rand = sortear(perks);
  var name = perks[rand].perk_name;
  $('#names').text(name);
}

// TODO: próximo passo de roulette.js : gravar winner com subname
async function pickWinner(subname){
  var s = await getCurrentSorteados();
  var rand = sortear(tokens);
  var perk_prot = tokens[rand].perk_prot;

  var i;
  var k;
  var j = 0;
  var protected = false;


  if(tokens[rand].perk_prot == -1){
    var stopper = 0;
    for (k = 0; k < sorteados.length; k++){
      if(sorteados[k].perk_name == tokens[rand].perk_name){
        k = 0;
        rand = sortear(tokens);
      }
      if(++stopper == 60) break;
    }
  }
  else{
    var stopper = 0;
    for(i = 0; i < perk_prot; i++){
      if(sorteados.last(++j) == tokens[rand]){
        i = 0;
        j = 0;
        rand = sortear(tokens);
      }
      if(++stopper == 60) break;
    }
  }

  console.log(tokens[rand]);
  return tokens[rand].perk_name;
}

function sortear(items){
  return Math.floor(Math.random() * items.length);
}

function rollClick() {
  $('#againBtn').on('click', function(e) {
    $(this).hide();
    setDeceleratingTimeout(function() { randomName() }, 10, 40);

    setTimeout(function() {
      var winner = pickWinner();
      $('#winner').hide();
      $('#winner').text(winner);
      $('#names').hide();
      $('#winner').show();
      $('#winner').html('<b>' + winner + '</b>');
      renderSorteados();
      $('#againBtn').fadeIn(2000);
    }, 8000); //aqui altera o tempo que a roleta fica girando (máx. 8000?)

    e.preventDefault();
  });
}

async function renderSorteados(){

  var sorted = await getCurrentSorteados();
  var i;
  for(i = 0; i < sorted.length; i++){
    $('#historico').prepend(`<div>${sorted[i].perk_name} - ${sorted[i].sorteado_subName} - ${sorted[i].sorteado_data}</div>`);
  }
}

function setDeceleratingTimeout(callback, factor, times) {
  var internalCallback = function(t, counter) {
    return function() {
      if (--t > 0) {
        window.setTimeout( internalCallback, ++counter * factor );
        callback();
      }
    }
  }(times, 0);

  window.setTimeout(internalCallback, factor);
};

function resetLayout(){
  var e = '';
  $('#winner').text(e);
  $('#names').text(e);
  $('#names').show();
  $('#roll').show();
}

//utilitários
function initialize(){
  if (!Array.prototype.last){
    Array.prototype.last = function(n){
      return this[this.length - n];
    };
  };
}

//debug
function debug(){
  // mockPerks();
  // mockSorteados();
  // getCurrentPerks();
  // wipeTable("sorteados");
  // perkByName("Ganso");
  getCurrentSorteados();
}

function perkByName(name){
  $.when(searchPerkByName(name)).then(
    function(status){
      // console.log("SearchPerkByName sucesso!");
      // console.log(status);
    },
    function(status){
      console.log("erro roulette.js.perkByName");
      console.log(status);
    },
    function(status){
      console.log("SearchPerkByName(status): "+status);
    }
  );//promise
}

function mockPerks(){
  var perksTemp = [
  {"perkName":"Lendária ou ban", "perkChance":"3","perkProt":"-1"},
  {"perkName":"10 minutos de TO", "perkChance":"8","perkProt":"-1"}, //2x
  {"perkName":"Escolha uma Imagem pro Chroma Key", "perkChance":"7","perkProt":"-1"},//2x
  {"perkName":"Pudim Battle Royale", "perkChance":"3","perkProt":"-1"},
  {"perkName":"Piada do saci", "perkChance":"3","perkProt":"-1"},
  {"perkName":"Jogo gratuito", "perkChance":"3","perkProt":"-1"},
  {"perkName":"dê TO em alguém", "perkChance":"8","perkProt":"-1"},//2x
  {"perkName":"Ganso", "perkChance":"4","perkProt":"-1"},
  {"perkName":"BG Temático", "perkChance":"3","perkProt":"-1"},
  {"perkName":"500 Rosecoins", "perkChance":"8","perkProt":"-1"}, //2x
  {"perkName":"Anúncio de graça", "perkChance":"8","perkProt":"-1"},//2x
  {"perkName":"Duelo com Tesdey", "perkChance":"7","perkProt":"-1"}, //2x
  {"perkName":"Roda 2x", "perkChance":"3","perkProt":"-1"},
  {"perkName":"Desenho na cara", "perkChance":"7","perkProt":"-1"},//2x
  {"perkName":"Frase de encerramento", "perkChance":"7","perkProt":"-1"},//2x
  {"perkName":"Add Emote BTTV", "perkChance":"4","perkProt":"-1"},
  {"perkName":"Jogar água na cabeça", "perkChance":"3","perkProt":"-1"},
  {"perkName":"Escolha 2 músicas", "perkChance":"3","perkProt":"-1"},
  {"perkName":"Pergunte ao Tesdey", "perkChance":"8","perkProt":"-1"}//2x
]
  var i;
  for(i = 0; i < perksTemp.length; i++){
    insertPerk(perksTemp[i]);
  }
}

function mockSorteados(){
  var sorteadosTemp = [
    {"perk": "Ganso", "subName": "Gabriel"},
    {"perk": "Lendária ou ban", "subName": "Jetulio"},
    {"perk": "Pergunte ao Tesdey", "subName": "legas"},
    {"perk": "Jogar água na cabeça", "subName": "MArcinho"},
    {"perk": "Frase de encerramento", "subName": "bonitro"},
    {"perk": "Roda 2x", "subName": "cakes"},
    {"perk": "500 Rosecoins", "subName": "jonas"},
    {"perk": "Escolha uma Imagem pro Chroma Key", "subName": "ulises"},
    {"perk": "dê TO em alguém", "subName": "larisa"},
    {"perk": "Ganso", "subName": "fones"},
    {"perk": "10 minutos de TO", "subName": "picas"},
    {"perk": "Ganso", "subName": "olivia"},
    {"perk": "10 minutos de TO", "subName": "resquiel"},
    {"perk": "Ganso", "subName": "jânus"},
    {"perk": "10 minutos de TO", "subName": "paçocas 123"},
  ];
  var i;
  for(i = 0; i < sorteadosTemp.length; i++){
    insertSorteio(sorteadosTemp[i]);
  }
}

async function getCurrentSorteados(){
  var resposta = jQuery.Deferred();
  var tempSorteados = [];
  $.when(getAllSorteados()).then(
    function(status){
      // console.log("getCurrentSorteados sucesso");
      // console.log(status);

      var i;
      for(i = 0; i < status.rows.length; i++){
        tempSorteados.push(status.rows[i]);
        // sorteadosLen++;
      }
      resposta.resolve(tempSorteados);
    },
    function(status){
      console.log("getCurrentSorteados erro");
      resposta.reject(status);
    },
    function(status){
      console.log("erro(status): "+status);
    }
  );//promise
  return resposta;
}

function getCurrentPerks(){
  $.when(getAllperks()).then(
    function(status){
      // console.log("getCurrentPerks sucesso");

      var i;
      // console.log(status);
      for(i = 0; i < status.rows.length; i++){
        perks.push(status.rows[i]);
        perkLen++;
      }
    },
    function(status){
      console.log("getCurrentPerks erro");
      console.log(status);
    },
    function(status){
      console.log("erro(status): "+status);
    }
  );//promise
}
