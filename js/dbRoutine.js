var db = openDatabase("perksDB", "1.0", "Perks DB", 2 * 1024 * 1024);

function initializeDB(){
    db.transaction(function(tx){
      tx.executeSql('CREATE TABLE IF NOT EXISTS perks'+
                    '(perk_id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, '+
                    'perk_name TEXT NOT NULL, '+
                    'perk_chance TEXT NOT NULL, '+
                    'perk_prot	TEXT NOT NULL)',
                    null,

                    function(transaction, result){
                      // console.log('Sucesso Transação: ');
                      // console.log(result);
                    },

                    function(transaction, error){
                        console.log('Erro initializeDB: ');
                        console.log(error);
                    }
        );//executeSql
        tx.executeSql('CREATE TABLE IF NOT EXISTS sorteados'+
                      '(perk_id INTEGER NOT NULL, '+
                      'perk_name TEXT NOT NULL, '+
                      'sorteado_id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, '+
                      'sorteado_subName TEXT NOT NULL, '+
                      'sorteado_data DATA NOT NULL)',
                      null,

                      function(transaction, result){
                        // console.log('Sucesso Transação: ');
                        // console.log(result);
                      },

                      function(transaction, error){
                          console.log('Erro initializeDB: ');
                          console.log(error);
                      }
          );//executeSql
    });
}

function insertPerk(perk){
  var name = perk.perkName;
  var chance = perk.perkChance;
  var prot = perk.perkProt;
  db.transaction(function (tx){
    tx.executeSql('INSERT OR REPLACE INTO perks(perk_name, perk_chance, perk_prot) values(?, ?, ?)',

      [[name],[chance],[prot]],

      function(tx, result){
        console.log('Sucesso Insert Perk: ');
        console.log(result);
      },

      function(tx, error){
          console.log('Erro insertPerk: ');
          console.log(error);
      }
    );
  });//db.transaction
}

function searchPerkByName(name){
  var resposta = jQuery.Deferred();
    db.transaction(function (tx){
      tx.executeSql('SELECT * FROM perks WHERE perk_name=?', [[name]],
      function(tx, result){
        // console.log("Sucesso SearchPerkByName");
        // console.log(result);
        resposta.resolve(result);
      },
      function(tx, error){
        console.log("Erro searchPerkByName: ");
        console.log(error);
        resposta.reject(error);
      });
    });//db.transaction
  return resposta.promise();
}

function wipeTable(table){
  db.transaction(function (tx){
    tx.executeSql('DELETE FROM ?',[[table]],
      function(tx, result){
        console.log("Sucesso delete");
      },
      function(tx, error){
        console.log("Erro delete"+error);
      }
    );
  });
}

function getAllperks(){
  var resposta = jQuery.Deferred();
  db.transaction(function (tx){
    tx.executeSql('SELECT * FROM perks', null,
    function(tx, result){
      // console.log("Sucesso Select");
      resposta.resolve(result);
    },
    function(tx, error){
      console.log("Erro getAllperks");
      resposta.reject(error);
    });
  });//db.transaction
  return resposta.promise();
}

function insertSorteio(s){
  var perkId;
  var perkName;
  $.when(searchPerkByName(s.perk)).then(
    function(status){
      perkId = status.rows[0].perk_id;
      perkName = status.rows[0].perk_name;

      var today = new Date();
      var sorteadoData = 'em '+today.getDate()+'/'+today.getMonth()+' às '+today.getHours()+'h'+today.getMinutes();



      db.transaction(function (tx){
        tx.executeSql('INSERT OR REPLACE INTO sorteados(perk_id, perk_name, sorteado_subName, sorteado_data) values(?, ?, ?, ?)',

          [[perkId], [perkName], [s.subName], [sorteadoData]],

          function(tx, result){
            console.log('Sucesso Insert Sorteio: ');
            console.log(result);
          },

          function(tx, error){
              console.log('Erro: ');
              console.log(error);
          }
        );
      });//db.transaction
    },
    function(status){
      console.log("erro [dbRoutine] searchPerkByName");
      console.log(status);
    },
    function(status){
      console.log("SearchPerkByName(status): "+status);
    }
  );//promise
}

function getAllSorteados(){
  var resposta = jQuery.Deferred();
  db.transaction(function (tx){
    tx.executeSql('SELECT * FROM sorteados', null,
    function(tx, result){
      // console.log("Sucesso Select");
      resposta.resolve(result);
    },
    function(tx, error){
      console.log("Erro getAllSorteados");
      resposta.reject(error);
    });
  });//db.transaction
  return resposta.promise();
}
